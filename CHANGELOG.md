## [1.12.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.12.2...v1.12.3) (2024-09-05)


### Bug Fixes

* **richdocuments:** Update to 8.4.6, ([ec647af](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/ec647af85ba9d75bfed4df0661807a8b58adffd5))

## [1.12.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.12.1...v1.12.2) (2024-09-04)


### Bug Fixes

* Nextcloud 29.0.6. ([a9ea442](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/a9ea4427ad4461776339b3ea64c7d57069ec4322))

## [1.12.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.12.0...v1.12.1) (2024-09-02)


### Bug Fixes

* **nextcloud_configure.php:** Add missing closing quote. ([936ca7f](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/936ca7fc5ba0b8c94891ba0a3b92c4027ec59076))

# [1.12.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.11.3...v1.12.0) (2024-09-02)


### Features

* Support for internal and external share expiry date settings. ([44d4cd3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/44d4cd34211445b60410009ac31d370c6497b32e))

## [1.11.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.11.2...v1.11.3) (2024-08-23)


### Bug Fixes

* **richdocuments:** Typo. ([6b4f193](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/6b4f19352454a433bf535b9f80138199da40025d))

## [1.11.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.11.1...v1.11.2) (2024-08-23)


### Bug Fixes

* **richdocuments:** OCC call for `activate-config`. ([935c51c](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/935c51c97d85ca1830122c2cc93488a366608bdc))

## [1.11.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.11.0...v1.11.1) (2024-08-21)


### Bug Fixes

* **sharing:** Update handling of share settings. ([0c36c03](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/0c36c03b277aa835e5be63a7a1729da963b316ae))

# [1.11.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.10.3...v1.11.0) (2024-08-21)


### Features

* Nextcloud 29.0.5. ([00a0a71](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/00a0a719cfb568e5155d170342bb221fded3a8ea))

## [1.10.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.10.2...v1.10.3) (2024-07-23)


### Bug Fixes

* **proxies:** Raise `opcache.interned_strings_buffer` limit, set maintenance_window and support wopi_allowlist. ([933c6e0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/933c6e0427d14dd2c528b4a9feca390980b866a9))

## [1.10.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.10.1...v1.10.2) (2024-07-16)


### Bug Fixes

* **wopi:** Allow internal WOPI URL to be set differently from external one. ([3313cc8](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/3313cc876cf5d827b3fbbe7958fb6f811faac49a))

## [1.10.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.10.0...v1.10.1) (2024-07-16)


### Bug Fixes

* **oidc:** Disable `selfencoded_bearer_validation_audience_check` in config.php as it breaks filepicker. ([e367cbe](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/e367cbefae160c8d249a3b6fdae2f477c7c1d01a))

# [1.10.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.9.1...v1.10.0) (2024-07-11)


### Features

* **src:** Introduce FS_ENV_MAIL_SMTPVERIFYPEER variable ([ba63624](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/ba636247ea4d95d0c2a7f322e16eda392ccd5199))

## [1.9.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.9.0...v1.9.1) (2024-07-03)


### Bug Fixes

* Update to 28.0.7 including latest apps for 28. ([33353ab](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/33353aba366a985f45bfc794e8dd493f3f3d268b))

# [1.9.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.8.12...v1.9.0) (2024-07-03)


### Features

* **retention:** Allow custom values for *_retention_obligation. ([8b9e0fb](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/8b9e0fb29c867159341676c279d263b57c6d1b93))

## [1.8.12](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.8.11...v1.8.12) (2024-07-02)


### Bug Fixes

* **Dockerfile:** Update versions to latest for bookworm ([55bb4ee](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/55bb4ee421b0a2c158a0edc68455fb6c4d3f91e7))
* **Dockerfile:** Update versions to latest for bookworm ([20aadc3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/20aadc39d6e2e43597ccc0f72991bd41a1aaa615))
* **php:** Set curl.cainfo and openssl.cafile to default ca cert path ([d69a110](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/d69a1108518ccf96dbc76828788aa5250baf481b))

## [1.8.11](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.8.10...v1.8.11) (2024-05-07)


### Bug Fixes

* **nextcloud:** Bump base-layer to 28.0.5 and latest 28-release apps ([c89b2cf](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/c89b2cfe5e2978fca9bd0a37921788123f04dd9f))

## [1.8.10](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.8.9...v1.8.10) (2024-05-06)


### Bug Fixes

* **integration_swp:** Bump to 3.1.16 ([f9e06d7](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/f9e06d77dea904ed0e5a82d73c9209551ed82a00))
* **php:** Bump to 8.2.18 ([f817e01](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/f817e01d43ff2884b37ae7f370e6d9c15abd4771))

## [1.8.9](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.8.8...v1.8.9) (2024-04-02)


### Bug Fixes

* **core:** Bump to 28.0.4 ([2ab7d04](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/2ab7d04457302f6a245406ae40b2f5540f5b43ac))

## [1.8.8](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.8.7...v1.8.8) (2024-03-28)


### Bug Fixes

* **sharing:** Set default share name to __Shared_with_me__ for better visibility ([2e75b84](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/2e75b844cf7ac94a991ee18930315cc9fff9722c))

## [1.8.7](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.8.6...v1.8.7) (2024-03-26)


### Bug Fixes

* **baselayer:** Bump to include NC 28.0.3 ([948d970](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/948d9706a76d747177a623b1c27469bc5431bffe))

## [1.8.6](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.8.5...v1.8.6) (2024-03-18)


### Bug Fixes

* **baselayer:** Bump to 1.4.7 ([f2051e8](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/f2051e86af246827f2a77a90cdd41d9232761eeb))

## [1.8.5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.8.4...v1.8.5) (2024-03-11)


### Bug Fixes

* **image:** Bump base layer ([603d9c8](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/603d9c8556a423e599bbfdaf880007304bc7cd5e))

## [1.8.4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.8.3...v1.8.4) (2024-02-27)


### Bug Fixes

* **apps:** Enable password_policy app and update base layer to 1.4.5 ([ffc24e6](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/ffc24e6dd9b9c66c77803a9159a0b20ebd1041ed))

## [1.8.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.8.2...v1.8.3) (2024-02-15)


### Bug Fixes

* **Dockerfile:** Use nextcloud artifacts as own layer to copy from to update via renovate bot ([aeeaaec](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/aeeaaecac104cdf9b170f3d20f1613ae1d72ad1e))

## [1.8.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.8.1...v1.8.2) (2024-02-15)


### Bug Fixes

* **Dockerfile:** Update debian:bookworm-slim Docker digest to 6bdbd57 ([e4394b4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/e4394b4e6291130eafefc0bdbff67410b69bfce9))

## [1.8.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.8.0...v1.8.1) (2024-02-14)


### Bug Fixes

* **apps:** Disable circles app ([5a1b1dc](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/5a1b1dc4b516e5bd9583e46b38e8be1fe65f3edf))

# [1.8.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.7.2...v1.8.0) (2024-02-13)


### Features

* **ldap:** Support for Admin role through custom group ([949c569](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/949c56980d3d66e125d9b7f1116e72d9a1559709))

## [1.7.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.7.1...v1.7.2) (2024-02-04)


### Bug Fixes

* **Dockerfile:** Update base-layer to 1.4.3 ([3266a94](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/3266a94d842971144fbbede062957e41200073c9))

## [1.7.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.7.0...v1.7.1) (2024-01-31)


### Bug Fixes

* **src:** Create .ocdata file before consistency check ([6b7d891](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/6b7d891b1400b824817cbce9e6fba23a8096f476))

# [1.7.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.6.3...v1.7.0) (2024-01-31)


### Features

* **src:** Create .ocdata file only when nextcloud is installed ([8ffad59](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/8ffad59275bdfd924675be59055d87cf5b21d74d))

## [1.6.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.6.2...v1.6.3) (2024-01-31)


### Bug Fixes

* **Dockerfile:** Update base to v1.4.2) ([db14d62](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/db14d62eff8b77b8674bf6598c0ef82da8c31e7f))

## [1.6.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.6.1...v1.6.2) (2024-01-30)


### Bug Fixes

* **Dockerfile:** Update base-layer to 1.4.1 ([6abf705](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/6abf7050f7f5dd437b8d3940552685e1ca5eb2bf))
* **src:** Remove logfile and use stdout ([cc03cd0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/cc03cd0bf46631c9a9e39557334e92b95ebbb2ae))

## [1.6.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.6.0...v1.6.1) (2024-01-03)


### Bug Fixes

* **src:** Fix missing semicolon in entrypoint_runtime ([f6f2711](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/f6f27119415b0c3cdf1e1dccff1b432feac279fc))

# [1.6.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.5.7...v1.6.0) (2024-01-03)


### Features

* **src:** Add config directory which will be copied to config mount on runtime ([3028f42](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/3028f42820e0927cd51503520ca2712fbe628ccd))

## [1.5.7](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.5.6...v1.5.7) (2023-12-28)


### Bug Fixes

* **Dockerfile:** Use Nextcloud base v1.3.4 ([7d93d2d](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/7d93d2d6b0cfc152177f02fc840b9a948a7f0bd2))

## [1.5.6](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/compare/v1.5.5...v1.5.6) (2023-12-28)


### Bug Fixes

* **Dockerfile:** Use Nextcloud base v1.3.3 ([90df481](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/commit/90df4810f16193f922bad07601908d5f7774eb43))

## [1.5.5](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.5.4...v1.5.5) (2023-12-28)


### Bug Fixes

* **src:** Add missing slash to ocdata location ([1ede8a7](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/1ede8a72df7846f28025817b54f8b7861109d388))

## [1.5.4](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.5.3...v1.5.4) (2023-12-28)


### Bug Fixes

* **src:** Change order of file creation ([fcb6af3](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/fcb6af36b7a690a7b880d9309fc7cf15e3cc9939))

## [1.5.3](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.5.2...v1.5.3) (2023-12-28)


### Bug Fixes

* **src:** Fix missing argument in file_put_contents ([e0344b4](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/e0344b499dc899d2e7b1afd086d7db56f3253cb4))

## [1.5.2](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.5.1...v1.5.2) (2023-12-27)


### Bug Fixes

* **src:** Move constant definition after function import ([dbec056](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/dbec056c77b2f9fe657b201e7d3cf9aab364831f))

## [1.5.1](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.5.0...v1.5.1) (2023-12-27)


### Bug Fixes

* **src:** Fix missing ) in CREATE_OCDATA_FILE constant ([433a9ac](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/433a9ace6f5f2a2e15b1e406561a5e4e7644a6ff))

# [1.5.0](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.4.10...v1.5.0) (2023-12-27)


### Features

* **src:** Add CREATE_OCDATA_FILE flag ([2be8650](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/2be8650e687282686b2da530abfbda37b5350140))

## [1.4.10](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.4.9...v1.4.10) (2023-12-27)


### Bug Fixes

* **Dockerfile:** Update Nextcloud artifacts to v1.3.1-nextcloud-27 ([ac6d027](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/ac6d027767756d6429618ec5235ee416f1d47905))
* **src:** Add FS_ENV_OIDC_AUTO_PROVISION parameter ([3719ae0](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/3719ae012a4e1cbbbc9471486490c1a96464cf65))

## [1.4.9](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.4.8...v1.4.9) (2023-12-24)


### Bug Fixes

* **src:** Fix SPDX entries in fontconfig ([f04e6aa](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/f04e6aa533b2c332652a29168f5e2d930d41f634))

## [1.4.8](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.4.7...v1.4.8) (2023-12-23)


### Bug Fixes

* **src:** Remove talk folder ([b84119f](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/b84119f597f26613b2e5782540269d58ba45483c))

## [1.4.7](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.4.6...v1.4.7) (2023-12-23)


### Bug Fixes

* **Dockerfile:** Update Nextcloud artifacts to v1.2.2 ([c7966bc](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/c7966bcce8b4a6cbad33a29297d88f2c8deba85c))

## [1.4.6](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.4.5...v1.4.6) (2023-12-22)


### Bug Fixes

* **src:** Update user_oidc configuration ([5950c4d](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/5950c4d7299a52f531f29e04a18b0839619f4dcc))

## [1.4.5](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.4.4...v1.4.5) (2023-12-22)


### Bug Fixes

* **Dockerfile:** Add ca-certificates to images ([992c201](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/992c201c80a8ecc6d073f8c14a547cce8807e169))

## [1.4.4](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.4.3...v1.4.4) (2023-12-22)


### Bug Fixes

* **src:** Fix unexpected brace ([b476b4c](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/b476b4c0e81f0a052ce7d3d7653173be032ee3b8))

## [1.4.3](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.4.2...v1.4.3) (2023-12-22)


### Bug Fixes

* **src:** Remove user_oidc_mapping_uid block ([02b3733](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/02b3733012a69be478ab4a43edf82273e655082a))

## [1.4.2](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.4.1...v1.4.2) (2023-12-22)


### Bug Fixes

* **src:** Remove unexpected semicolon ([1c18721](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/1c18721e1ca7bffbfe3f980dba421e2e67494682))

## [1.4.1](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.4.0...v1.4.1) (2023-12-22)


### Bug Fixes

* **src:** Fix array syntax in config_base.php ([495effa](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/495effaaf5faa5187c1e9a19bc4038a4f234acea))

# [1.4.0](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.3.2...v1.4.0) (2023-12-21)


### Bug Fixes

* **src:** Remove files_rightclick, cause deprecated from v28 ([8d0e4a7](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/8d0e4a74945360580155712cb63fcacb2081cd5a))


### Features

* **src:** Add Cryptpad diagrams feature ([4d04d43](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/4d04d4327f79c2adddf33961af93b562268d77f1))
* **src:** Add objectstore configuration ([a48c9b8](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/a48c9b8006c92f3710eaf83e317314f043322462))

## [1.3.2](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.3.1...v1.3.2) (2023-12-21)


### Bug Fixes

* **Dockerfile:** Update nextcloud artifacts to v1.2.0 ([9d8f1de](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/9d8f1dea3dba6083c410155a6245797b9cdfecbc))

## [1.3.1](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.3.0...v1.3.1) (2023-12-21)


### Bug Fixes

* **src:** Fix wrong variable name in entrypoint ([e4029ed](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/e4029ed4684ce46d26247a1420803c27800c7919))

# [1.3.0](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.2.0...v1.3.0) (2023-12-21)


### Features

* **src:** Add FS_ENV_OIDC_MAPPING_UID option ([41a50a7](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/41a50a793ebd7811953271a1334cca244cb3b995))

# [1.2.0](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.1.5...v1.2.0) (2023-12-21)


### Features

* **src:** Add first bunch of openDesk related configuration changes ([847ecd0](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/847ecd04ce309af626254f2a54070dd98558e5e2))

## [1.1.5](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.1.4...v1.1.5) (2023-12-20)


### Bug Fixes

* **src:** Fix missing bracket in create dir ([820148a](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/820148afdce7482bb339b5d3bfb5d65f7a7f9116))

## [1.1.4](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.1.3...v1.1.4) (2023-12-20)


### Bug Fixes

* **src:** Create data directory in mount if not exists ([d18fb3b](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/d18fb3b87129e56b12898c244832e12a3d1167a1))

## [1.1.3](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.1.2...v1.1.3) (2023-12-20)


### Bug Fixes

* **Dockerfile:** Add missing path creation ([f6945df](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/f6945df28c50a49f538127940f8e367ccb9b73b3))
* **Dockerfile:** Change pathes of nextcloud data directories ([b3599a4](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/b3599a465d6388bb0385affec5c14d013ca355f3))

## [1.1.2](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.1.1...v1.1.2) (2023-12-19)


### Bug Fixes

* **Dockerfile:** Update opendesk-nextcloud-artifacts to v1.1.2 ([a997e73](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/a997e73fb5f3eec756c22030158b2a5453133cd8))

## [1.1.1](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.1.0...v1.1.1) (2023-12-18)


### Bug Fixes

* **opendesk-nextcloud-php:** Add missing Nextcloud artifacts ([2979435](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/29794354c963cbafb93c3a667cbef93c5a508e1e))

# [1.1.0](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/compare/v1.0.0...v1.1.0) (2023-12-18)


### Features

* **opendesk-nextcloud-php:** Remove PHP layer ([e334014](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/e3340144e5748e6c8e5e9089791b6da0fbf19293))

# 1.0.0 (2023-12-16)


### Bug Fixes

* **Dockerfile:** Only copy /var dir to avoid recursion in kaniko ([2cdfe8b](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/2cdfe8b9f123ed2ef0829c9b8c15418fbccbe972))
* **Dockerfile:** Use image in COPY --from command and use relative path ([9fb8fc5](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/9fb8fc5a9542f5c0ce5a0bd5a2cc5e254f272ba4))
* **opendesk-nextcloud-php:** Initial commit ([b497f15](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php/commit/b497f1582fad87b5b5fb1fd747b21679f96bf604))
