# SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-FileCopyrightText: 2023 Dataport AöR
# SPDX-License-Identifier: EUPL-1.2
FROM registry.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-base-layer:1.5.2@sha256:5966366aea510e472847fcffc6d41f1d84dfb9f5e5b21994bf09bece602ce8b1 AS artifacts

FROM docker.io/debian:bookworm-slim@sha256:84d83b22ba6c367e143fcb7169717d87d7f484356cf9a904f5352418981a99a3 AS build

LABEL org.opencontainers.image.authors="Bundesministerium des Innern und für Heimat" \
      org.opencontainers.image.documentation=https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php/-/blob/main/README.md \
      org.opencontainers.image.source=https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php \
      org.opencontainers.image.vendor="Bundesministerium des Innern und für Heimat" \
      org.opencontainers.image.licenses=EUPL-1.2

ARG PHP_VERSION="8.2"

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN apt-get update \
 && apt-get -y install --no-install-recommends \
    python3=3.11.2-1+b1 \
    imagemagick=8:6.9.11.60+dfsg-1.6+deb12u1 \
    php${PHP_VERSION}-fpm=8.2.20-1~deb12u1 \
    php${PHP_VERSION}-gd=8.2.20-1~deb12u1 \
    php${PHP_VERSION}-mysql=8.2.20-1~deb12u1 \
    php${PHP_VERSION}-curl=8.2.20-1~deb12u1 \
    php${PHP_VERSION}-mbstring=8.2.20-1~deb12u1 \
    php${PHP_VERSION}-intl=8.2.20-1~deb12u1 \
    php${PHP_VERSION}-gmp=8.2.20-1~deb12u1 \
    php${PHP_VERSION}-bcmath=8.2.20-1~deb12u1 \
    php${PHP_VERSION}-xml=8.2.20-1~deb12u1 \
    php${PHP_VERSION}-zip=8.2.20-1~deb12u1 \
    php${PHP_VERSION}-ldap=8.2.20-1~deb12u1 \
    php${PHP_VERSION}-pgsql=8.2.20-1~deb12u1 \
    php${PHP_VERSION}-redis=5.3.7+4.3.0-3 \
    php${PHP_VERSION}-apcu=5.1.22+4.0.11-2 \
    php${PHP_VERSION}-imagick=3.7.0-4 \
    libfcgi-bin=2.4.2-2 \
    ca-certificates=20230311 \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 && adduser --uid 65532 --shell /sbin/nologin --home /nonexistent --no-create-home --disabled-password --gecos "" nonroot

WORKDIR /var/www/html/

RUN mkdir /output; mkdir -p /var/nextcloud/data; mkdir -p /var/nextcloud/config; rm -rf /var/www/html/* \
 && ln -s /usr/sbin/php-fpm${PHP_VERSION} /usr/sbin/php-fpm \
 && echo "php_version=\"$(php -v | grep -i 'PHP' | head -n 1 | awk '{print $2}')\"" > /php_version.txt \
 && chown -R 65532:65532 /var/www/html/ /var/nextcloud/data/ /var/nextcloud/config/ \
 && touch /var/nextcloud/data/dummyfile \
 && touch /var/nextcloud/config/.keep

COPY src/files/etc/php/ /etc/php/${PHP_VERSION}/
COPY src/files/etc/fonts/conf.d/ /etc/fonts/conf.d/
COPY src/files/usr/local/bin/entrypoint/ /usr/local/bin/entrypoint/
COPY src/files/var/www/html/config.d/ /var/www/html/config.d/

COPY --from=artifacts var/www /var/www

USER nonroot:nonroot
ENV LANG de_DE.UTF-8
STOPSIGNAL SIGQUIT
EXPOSE 9000
HEALTHCHECK CMD SCRIPT_NAME=/ping SCRIPT_FILENAME=/ping REQUEST_METHOD=GET cgi-fcgi -bind -connect localhost:9000
CMD [ "/usr/bin/php", "/usr/local/bin/entrypoint/entrypoint.php" ]
