<!--
SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-License-Identifier: Apache-2.0
-->

# openDesk Nextcloud images

The openDesk Nextcloud images are a set of depended images.

Please visit the [central documentation in the *Base* repository](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-base-layer/-/blob/main/README.md)
for more information.
