<?php
// SPDX-FileCopyrightText: 2023 Dataport AöR
// SPDX-License-Identifier: EUPL-1.2
require_once "config_base.php";

$config_php_current = config_php_load_from_db(true);

// apply config from env vars, see config_base.php
foreach ($CONFIG_BASE as $k => $v) {
    $config_php_current[$k] = $v;
}

// sort array by key
ksort($config_php_current);

config_php_save_to_file($config_php_current);
