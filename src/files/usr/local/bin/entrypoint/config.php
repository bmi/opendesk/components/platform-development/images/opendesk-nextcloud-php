<?php
// SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
// SPDX-FileCopyrightText: 2023 Dataport AöR
// SPDX-License-Identifier: EUPL-1.2

// purposes that need to be fulfilled by the config_struct
$config_purposes_required = ["antivirus"];


// this structure contains app configurations based on feature flags and environment variables
$config_struct = [];

$config_struct["subscription_offline"] = [
    "purpose" => "subscription",

    "env_vars" => [
        "FS_ENV_SUBSCRIPTION_KEY",
        "FS_ENV_SUBSCRIPTION_DATA"
    ],

    "occ_enable" => [
        "config:app:set support subscription_key --value '" . trim(get_from_env("FS_ENV_SUBSCRIPTION_KEY")) . "'",
        "config:app:set support last_response --value '" . trim(get_from_env("FS_ENV_SUBSCRIPTION_DATA")) . "'",
        "config:app:set support last_error --value ''",
        "config:app:set support last_check --value " . time() . " --type integer",
        "config:system:set has_internet_connection --value false --type boolean"
    ]
];

$config_struct["subscription_online"] = [
    "purpose" => "subscription",

    "env_vars" => [
        "FS_ENV_SUBSCRIPTION_KEY",
    ],

    "occ_enable" => [
        "config:app:set support subscription_key --value '" . trim(get_from_env("FS_ENV_SUBSCRIPTION_KEY")) . "'",
        "config:app:set support last_error --value ''",
    ]
];

$config_struct["integration_swp_no_groupware"] = [

    // feature flags to watch for
    "feature_flags" => [
        "no_groupware" => true // FS_ENV_FEATURE_NO_GROUPWARE=true
    ],

    // required env-vars
    "env_vars" => [
        "FS_ENV_PORTAL_SECRET",
        "FS_ENV_PORTAL_NAVIGATION_JSON_URL",
        "FS_ENV_PORTAL_AUTH_METHOD",
        "FS_ENV_PORTAL_JSON_USERNAME_ATTRIBUTE",
        "FS_ENV_PORTAL_MENU_TABNAME",
        "FS_ENV_PORTAL_NAVIGATION_JSON_TTL"
    ],

    // purpose that his block fulfills. if a purpose is already fulfilled by block a, block b (that fulfills the same purpose) is not executed
    "purpose" => "integration_swp",

    // desired app states. are combined before execution, last one has priority
    "app_states" => [
        "integration_swp" => true,
        "contacts" => true,
        "circles" => false,
    ],

    // commands to execute to enable this block
    "occ_enable" => [
        "config:app:set integration_swp navigation-json-api-secret --value '" . get_from_env("FS_ENV_PORTAL_SECRET") . "'",
        "config:app:set integration_swp navigation-json-url --value '" . get_from_env("FS_ENV_PORTAL_NAVIGATION_JSON_URL") . "'",
        "config:app:set integration_swp navigation-json-auth-type --value '" . get_from_env("FS_ENV_PORTAL_AUTH_METHOD") . "'",
        "config:app:set integration_swp navigation-json-username-attribute --value '" . get_from_env("FS_ENV_PORTAL_JSON_USERNAME_ATTRIBUTE") . "'",
        "config:app:set integration_swp menu-tabname-attribute --value '" . get_from_env("FS_ENV_PORTAL_MENU_TABNAME") . "'",
        "config:app:set integration_swp webmail-url --value ''",
        "config:app:set integration_swp webmail-tabname --value ''",
        "config:app:set integration_swp ox-baseurl --value ''",
        "config:app:set integration_swp cache-navigation-json --value " . get_from_env("FS_ENV_PORTAL_NAVIGATION_JSON_TTL"),
        "config:app:set integration_swp square-corners --value='1'"
    ],
];


$config_struct["integration_swp"] = [
    "feature_flags" => [],
    "env_vars" => [
        "FS_ENV_PORTAL_URL",
        "FS_ENV_LOGO_URL",
        "FS_ENV_WEBMAIL_URL",
        "FS_ENV_PORTAL_SECRET",
        "FS_ENV_PORTAL_NAVIGATION_JSON_URL",
        "FS_ENV_PORTAL_AUTH_METHOD",
        "FS_ENV_PORTAL_JSON_USERNAME_ATTRIBUTE",
        "FS_ENV_PORTAL_MENU_TABNAME"
    ],
    "purpose" => "integration_swp",

    "app_states" => [
        "integration_swp" => true,
    ],

    "occ_enable" => [
        "config:app:set integration_swp navigation-json-api-secret --value '" . get_from_env("FS_ENV_PORTAL_SECRET") . "'",
        "config:app:set integration_swp navigation-json-url --value '" . get_from_env("FS_ENV_PORTAL_NAVIGATION_JSON_URL") . "'",
        "config:app:set integration_swp navigation-json-auth-type --value '" . get_from_env("FS_ENV_PORTAL_AUTH_METHOD") . "'",
        "config:app:set integration_swp navigation-json-username-attribute --value '" . get_from_env("FS_ENV_PORTAL_JSON_USERNAME_ATTRIBUTE") . "'",
        "config:app:set integration_swp menu-tabname-attribute --value '" . get_from_env("FS_ENV_PORTAL_MENU_TABNAME") . "'",
        "config:app:set integration_swp webmail-url --value '" . get_from_env("FS_ENV_WEBMAIL_URL") . "'",
        "config:app:set integration_swp webmail-tabname --value 'tab_groupware'",
        "config:app:set integration_swp ox-baseurl --value '" . get_from_env("FS_ENV_WEBMAIL_URL") . "/appsuite'",
        "config:app:set integration_swp cache-navigation-json --value '5'",
        "config:app:set integration_swp square-corners --value '1'",
        "config:app:set integration_swp use-custom-logo --value '1'",
        "config:app:set integration_swp logo-image-url --value '" . get_from_env("FS_ENV_LOGO_URL") . "'",
        "config:app:set integration_swp logo-width --value '63%'",
        "config:app:set integration_swp logo-height --value 'auto'",
        "config:app:set integration_swp logo-link-url --value '" . get_from_env("FS_ENV_PORTAL_URL") . "'",
        "config:app:set integration_swp logo-link-target --value 'swp-portal'",
        "config:app:set integration_swp logo-link-title --value 'Portal'"
    ]
];


$config_struct["user_oidc"] = [
    "env_vars" => [
        "FS_ENV_OIDC_CLIENT_ID",
        "FS_ENV_OIDC_CLIENT_SECRET",
        "FS_ENV_OIDC_DISCOVERY_URI"
    ],

    "app_states" => [
        "user_oidc" => true,
    ],

    "occ_enable" => [
        "user_oidc:provider opendesk --clientid '" . get_from_env("FS_ENV_OIDC_CLIENT_ID") . "' --clientsecret '" . get_from_env("FS_ENV_OIDC_CLIENT_SECRET") . "' --discoveryuri '" . get_from_env("FS_ENV_OIDC_DISCOVERY_URI") ."'",
        "user_oidc:provider opendesk --scope '" . get_from_env("FS_ENV_OIDC_SCOPE", "openid") . "' --check-bearer=true --unique-uid=0 --mapping-uid=" . get_from_env("FS_ENV_OIDC_MAPPING_UID", "opendesk_useruuid"),
        "user_oidc:provider opendesk --extra-claims=" . get_from_env("FS_ENV_OIDC_EXTRA_CLAIMS", "scope") . "",
        "config:app:set user_oidc allow_multiple_user_backends --value=0"
    ]
];

$antivirus_action = get_from_env("FS_ENV_ANTIVIRUS_ACTION", "delete");
$config_struct["antivirus_daemon"] = [
    "env_vars" => [
        "FS_ENV_ANTIVIRUS_CLAMAV_HOST",
        "FS_ENV_ANTIVIRUS_CLAMAV_PORT"
    ],

    "purpose" => "antivirus",

    "app_states" => [
        "files_antivirus" => true,
    ],

    "occ_enable" => [
        "config:app:set files_antivirus av_infected_action --value=$antivirus_action",
        "config:app:set files_antivirus av_max_file_size --value=-1",
        "config:app:set files_antivirus av_mode --value='daemon'",
        "config:app:set files_antivirus av_host --value='" . get_from_env("FS_ENV_ANTIVIRUS_CLAMAV_HOST") . "'",
        "config:app:set files_antivirus av_port --value='" . get_from_env("FS_ENV_ANTIVIRUS_CLAMAV_PORT") . "'",
        "config:app:set files_antivirus av_stream_max_length --value=26214400"
    ]
];


$config_struct["antivirus_icap"] = [
    "env_vars" => [
        "FS_ENV_ANTIVIRUS_ICAP_HOST",
        "FS_ENV_ANTIVIRUS_ICAP_PORT"
    ],

    "purpose" => "antivirus",

    "app_states" => [
        "files_antivirus" => true,
    ],

    "occ_enable" => [
        "config:app:set files_antivirus av_infected_action --value=$antivirus_action",
        "config:app:set files_antivirus av_max_file_size --value=-1",
        "config:app:set files_antivirus av_mode --value='icap'",
        "config:app:set files_antivirus av_stream_max_length --value=26214400",
        "config:app:set files_antivirus av_host --value=" . get_from_env("FS_ENV_ANTIVIRUS_ICAP_HOST"),
        "config:app:set files_antivirus av_port --value=" . get_from_env("FS_ENV_ANTIVIRUS_ICAP_PORT"),
        "config:app:set files_antivirus av_icap_request_service --value=" . get_from_env("FS_ENV_ANTIVIRUS_ICAP_SERVICE", "avscan"),
        "config:app:set files_antivirus av_icap_response_header --value=" . get_from_env("FS_ENV_ANTIVIRUS_ICAP_HEADER", "X-Infection-Found"),
        "config:app:set files_antivirus av_icap_mode --value=" . get_from_env("FS_ENV_ANTIVIRUS_ICAP_MODE", "reqmod"),
        "config:app:set files_antivirus av_icap_chunk_size --value=" . get_from_env("FS_ENV_ANTIVIRUS_ICAP_CHUNK_SIZE", 1048576),
    ]
];

$config_struct["cryptpad"] = [
    "env_vars" => [
        "FS_ENV_CRYPTPAD_HOST"
    ],

    "purpose" => "cryptpad",

    "app_states" => [
        "openincryptpad" => true
    ],

    "occ_enable" => [
        "config:app:set openincryptpad CryptPadUrl:diagram --value=" . get_from_env("FS_ENV_CRYPTPAD_HOST")
    ]
];


$config_struct["notify_push"] = [
    "env_vars" => [
        "FS_ENV_NOTIFY_PUSH_URL"
    ],

    "app_states" => [
        "notify_push" => true,
    ],

    "occ_enable" => [
        "config:app:set notify_push base_endpoint --value '" . get_from_env("FS_ENV_NOTIFY_PUSH_URL") . "'"
    ]
];


$config_struct["richdocuments"] = [
    "env_vars" => [
        "FS_ENV_INTERNAL_WOPI_URL",
        "FS_ENV_PUBLIC_WOPI_URL",
        "FS_ENV_WOPI_ALLOWLIST"
    ],

    "app_states" => [
        "richdocuments" => true,
    ],

    "occ_enable" => [
        "config:app:set richdocuments wopi_url --value '" . get_from_env("FS_ENV_INTERNAL_WOPI_URL") . "'",
        "config:app:set richdocuments public_wopi_url --value '" . get_from_env("FS_ENV_PUBLIC_WOPI_URL") . "'",
        "config:app:set richdocuments wopi_allowlist --value '" . get_from_env("FS_ENV_WOPI_ALLOWLIST") . "'",
    ]
];


$config_struct["monitoring"] = [
    "env_vars" => [
        "FS_ENV_MONITORING_TOKEN"
    ],

    "app_states" => [
        "serverinfo" => true
    ],

    "occ_enable" => [
        "config:app:set serverinfo token --value '" . get_from_env("FS_ENV_MONITORING_TOKEN") ."'"
    ]
];


$config_struct["federation_enabled"] = [
    "feature_flags" => [
        "federation_enabled" => true
    ],

    "purpose" => "federation_enabled",

    "app_states" => [
        "federation" => true
    ],

    "occ_enable" => [
        "config:app:set files_sharing incoming_server2server_share_enabled --value='yes'",
        "config:app:set files_sharing outgoing_server2server_share_enabled --value='yes'",
    ]
];


$config_struct["federation_disabled"] = [
    "feature_flags" => [
        "federation_enabled" => false
    ],

    "purpose" => "federation_enabled",

    "app_states" => [
        "federation" => false
    ],

    "occ_enable" => [
        "config:app:set files_sharing incoming_server2server_share_enabled --value='no'",
        "config:app:set files_sharing outgoing_server2server_share_enabled --value='no'",
    ]
];


$config_struct["imaginary"] = [
    "env_vars" => [
        "FS_ENV_IMAGINARY_URL"
    ],

    "occ_enable" => [
        "config:system:set enabledPreviewProviders 0 --value 'OC\\Preview\\Imaginary'",
        "config:system:set preview_imaginary_url --value '" . get_from_env("FS_ENV_IMAGINARY_URL") . "'"
    ]
];



// returns:
/*
[
    "app_states" => [
        "user_oidc" => true,
        "contacts" => false
    ],
    "commands" => [
        "user_oidc" => [
            "config:system:set user_oidc auto_provision --value=false --type=boolean",
            "user_oidc:provider ncoidc --mapping-uid=entryuuid --scope 'openid' --check-bearer=true --unique-uid=0"
        ],
        "notify_push" => [
            "config:app:set notify_push base_endpoint --value 'blablablub'"
        ]
    ]
]
*/
function render_config_struct($config_struct, $purposes_required = []) {
    $purposes_found = [];
    $final_app_states = [];
    $final_commands = [];
    $disabled_blocks = [];

    foreach ($config_struct as $block_name => $block) {
        print(PHP_EOL . "Evaluating block $block_name..." . PHP_EOL);

        if (isset($block["feature_flags"])) {
            foreach ($block["feature_flags"] as $flag => $flag_value) {
                $env_var_name = "FS_ENV_FEATURE_" . strtoupper($flag);
                if (get_from_env($env_var_name, false) != $flag_value) {
                    print("Flag $flag has not the required state (required: $flag_value). Skipping block." . PHP_EOL . PHP_EOL);
                    $disabled_blocks[] = $block_name;
                    continue 2;
                }
            }
        }

        if (isset($block["env_vars"])) {
            foreach ($block["env_vars"] as $env_var) {
                if (get_from_env($env_var) === false) { // TODO: use new getenv
                    print("Env var $env_var is not defined. Skipping block." . PHP_EOL . PHP_EOL);
                    $disabled_blocks[] = $block_name;
                    continue 2;
                }
            }
        }

        if (isset($block["purpose"])) {
            if (in_array($block["purpose"], $purposes_found)) {
                print("Purpose " . $block["purpose"] . " already fulfilled. Skipping block." . PHP_EOL . PHP_EOL);
                $disabled_blocks[] = $block_name;
                continue;
            }
        }

        print("Requirements for block $block_name fulfilled. Doing things now." . PHP_EOL);
        if (isset($block["app_states"])) {
            foreach ($block["app_states"] as $app_name => $app_state) {
                $final_app_states[$app_name] = $app_state;
            }
        }

        if (isset($block["occ_enable"])) {
            $final_commands[$block_name] = $block["occ_enable"] ;
        }

        if (isset($block["purpose"])) {
            $purposes_found[] = $block["purpose"];
        }

        print("Done with $block_name." . PHP_EOL);
    }

    $purposes_missing = [];
    foreach ($purposes_required as $purpose) {
        if (!in_array($purpose, $purposes_found)) {
            $purposes_missing[] = $purpose;
        }
    }

    if (count($purposes_missing) > 0) {
        print("ERROR: The following purposes where not enabled: " . implode(", ", $purposes_missing) . PHP_EOL);
        if (DEV_MODE) {
            print("Not exiting here, we are in dev mode." . PHP_EOL);
        } else {
            exit(1); //TODO: Define exit code
        }
    }

    // get commands for disabled blocks if no other block with that purpose was enabled
    foreach ($disabled_blocks as $block_name) {
        $block = $config_struct[$block_name];
        if (isset($block["purpose"])) {
            if (in_array($block["purpose"], $purposes_found)) {
                continue;
            }
        }

        if (isset($block["occ_disable"])) {
            $final_commands[$block_name] = $block["occ_disable"];
        }
    }

    print("Final app states:" . PHP_EOL);
    foreach ($final_app_states as $app => $state) {
        print(" $app: $state" . PHP_EOL);
    }
    print(PHP_EOL);

    print("OCC-Commands:" . PHP_EOL);
    foreach ($final_commands as $block_name => $cmds) {
        print("- $block_name" . PHP_EOL);
        foreach ($cmds as $cmd) {
            print(" $cmd" . PHP_EOL);
        }

    }

    print(PHP_EOL);

    return [
        "app_states" => $final_app_states,
        "commands" => $final_commands
    ];
}
