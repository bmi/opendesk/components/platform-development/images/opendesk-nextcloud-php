<?php
// SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
// SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
// SPDX-FileCopyrightText: 2023 Dataport AöR
// SPDX-License-Identifier: EUPL-1.2
require_once "functions.php";

// this is the base nextcloud config.php, these values get written into the config.php on every init run

$CONFIG_BASE = [];

$CONFIG_BASE['datadirectory'] = NC_DATA_DIR;
$CONFIG_BASE['dbtype'] = get_from_env('FS_ENV_DBTYPE');
$CONFIG_BASE['dbname'] = get_from_env('FS_ENV_DBNAME');
$CONFIG_BASE['dbhost'] = get_from_env('FS_ENV_DBHOST') . ":" . intval(get_from_env('FS_ENV_DBPORT', 3306));
$CONFIG_BASE['dbuser'] = get_from_env('FS_ENV_DBUSER');
$CONFIG_BASE['dbpassword'] = get_from_env('FS_ENV_DBPASSWORD');
$CONFIG_BASE['mysql.utf8mb4'] = str2bool(get_from_env('FS_ENV_DBENCODING', true));
$CONFIG_BASE['loglevel'] = get_from_env("FS_ENV_LOGLEVEL", 1);
$CONFIG_BASE['log_type'] = 'file';
$CONFIG_BASE['logfile'] = get_from_env('FS_ENV_LOGFILE', "/dev/stdout");
$CONFIG_BASE['log_rotation'] = intval(get_from_env('FS_ENV_LOGROTATION', 104857600));
$CONFIG_BASE['log_type_audit'] = 'file';
$CONFIG_BASE['logfile_audit'] = get_from_env('FS_ENV_LOGFILE_AUDIT', "/dev/stdout");
$CONFIG_BASE['tempdirectory'] = NC_DATA_DIR . '/tmp';
$CONFIG_BASE['redis'] = array (
    'host' => get_from_env('FS_ENV_REDIS_HOST'),
    'port' => intval(get_from_env('FS_ENV_REDIS_PORT', 6379)),
    'timeout' => get_from_env('FS_ENV_REDIS_TIMEOUT', 30),
    'dbindex' => intval(get_from_env('FS_ENV_REDIS_DBINDEX', 0)),
    'password' => get_from_env('FS_ENV_REDIS_PASSWORD', "")
);
$CONFIG_BASE['objectstore'] = array (
    'class' => get_from_env('FS_ENV_OBJECTSTORE_CLASS', '\\OC\\Files\\ObjectStore\\S3'),
    'arguments' => array (
        'bucket' => get_from_env('FS_ENV_OBJECTSTORE_BUCKET'),
        'hostname' => get_from_env('FS_ENV_OBJECTSTORE_HOST'),
        'key' => get_from_env('FS_ENV_OBJECTSTORE_ACCESS_KEY'),
        'secret' => get_from_env('FS_ENV_OBJECTSTORE_SECRET_KEY'),
        'port' => intval(get_from_env('FS_ENV_OBJECTSTORE_PORT', 443)),
        'use_path_style' => str2bool(get_from_env('FS_ENV_OBJECTSTORE_PATH_STYLE', true)),
        'use_ssl' => str2bool(get_from_env('FS_ENV_OBJECTSTORE_USE_SSL', true)),
        'region' => get_from_env('FS_ENV_OBJECTSTORE_REGION', "eu-west-1"),
        'storageClass' => get_from_env('FS_ENV_OBJECTSTORE_STORAGE_CLASS', "STANDARD")
    )
);
if (get_from_env("FS_ENV_REDIS_USER") !== false) {
    $CONFIG_BASE['redis']['user'] = get_from_env("FS_ENV_REDIS_USER");
} elseif (isset($FCONFIG_BASE['redis']['user'])) {
    unset($CONFIG_BASE['redis']['user']);
}

$CONFIG_BASE['memcache.distributed'] = '\\OC\\Memcache\\Redis';
$CONFIG_BASE['memcache.locking'] = '\\OC\\Memcache\\Redis';
//$CONFIG_BASE['auth.bruteforce.protection.enabled'] = true;
$CONFIG_BASE['skeletondirectory'] = '';
$CONFIG_BASE['defaultapp'] = 'file';
$CONFIG_BASE['activity_expire_days'] = 14;
$CONFIG_BASE['updater.release.channel'] = 'stable';
$CONFIG_BASE['theme'] = '';
$CONFIG_BASE['appstoreenabled'] = false;
$CONFIG_BASE['allow_local_remote_servers'] = true;
$CONFIG_BASE['filelocking.enabled'] = true;
$CONFIG_BASE['enable_previews'] = true;
$CONFIG_BASE['integrity.check.disabled'] = false;
$CONFIG_BASE['share_folder'] = '/__Shared_with_me__';
$CONFIG_BASE['memcache.local'] = '\\OC\\Memcache\\APCu';
$CONFIG_BASE['preview_max_x'] = 1024;
$CONFIG_BASE['preview_max_y'] = 768;
$CONFIG_BASE['preview_max_scale_facto'] = 1;
$CONFIG_BASE['default_language'] = 'de_DE';
$CONFIG_BASE['default_locale'] = 'de_DE';
$CONFIG_BASE['logtimezone'] = 'Europe/Berlin';
$CONFIG_BASE['maintenance_window_start'] = 1;
$CONFIG_BASE['versions_retention_obligation'] = get_from_env('FS_ENV_RETENTION_OBLIGATION_VERSIONS', '30,30');
$CONFIG_BASE['trashbin_retention_obligation'] = get_from_env('FS_ENV_RETENTION_OBLIGATION_TRASHBIN', '30,30');
$CONFIG_BASE['upgrade.disable-web'] = true;
$CONFIG_BASE['htaccess.RewriteBase'] = '/';
$CONFIG_BASE['mail_smtppassword'] = get_from_env('FS_ENV_MAIL_SMTPPASSWORD', "");
$CONFIG_BASE['mail_sendmailmode'] = get_from_env('FS_ENV_MAIL_SENDMAILMODE', "");
$CONFIG_BASE['mail_from_address'] = get_from_env('FS_ENV_MAIL_FROM_ADDRESS', "");
$CONFIG_BASE['mail_smtpmode'] = get_from_env('FS_ENV_MAIL_SMTPMODE', "");
$CONFIG_BASE['mail_smtpauthtype'] = get_from_env('FS_ENV_MAIL_SMTPAUTHTYPE', "");
$CONFIG_BASE['mail_domain'] = get_from_env('FS_ENV_MAIL_DOMAIN', "");
$CONFIG_BASE['mail_smtpname'] = get_from_env('FS_ENV_MAIL_SMTPNAME', "");
$CONFIG_BASE['mail_smtpsecure'] = get_from_env('FS_ENV_MAIL_SMTPSECURE', "");
$CONFIG_BASE['mail_smtpauth'] = get_from_env('FS_ENV_MAIL_SMTPAUTH', "");
$CONFIG_BASE['mail_smtphost'] = get_from_env('FS_ENV_MAIL_SMTPHOST', "");
$CONFIG_BASE['mail_smtpport'] = get_from_env('FS_ENV_MAIL_SMTPPORT', "");
if (str2bool(get_from_env('FS_ENV_MAIL_SMTPVERIFYPEER', true)) !== false) {
    $CONFIG_BASE['mail_smtpstreamoptions'] = array (
        'ssl' => array (
            'allow_self_signed' => true,
            'verify_peer' => false,
            'verify_peer_name' => false
        )
    );
}
$CONFIG_BASE['user_oidc'] = [
    'auto_provision' => str2bool(get_from_env('FS_ENV_OIDC_AUTO_PROVISION', false)),
    'selfencoded_bearer_validation_audience_check' => str2bool(get_from_env('FS_ENV_OIDC_SELFENCODED_BEARER_VALIDATION_AUDIENCE_CHECK', false))
];
$CONFIG_BASE['profile.enabled'] = false;
$CONFIG_BASE['config_is_read_only'] = false;
$CONFIG_BASE['maintenance'] = false;
$CONFIG_BASE['updatechecker'] = false;

// fixed indices starting at 1 to be able to insert Imaginary as preview provider at index 0
$default_preview_providers = [
    1 => 'OC\\Preview\\PNG',
    2 => 'OC\\Preview\\JPEG',
    3 => 'OC\\Preview\\GIF',
    4 => 'OC\\Preview\\BMP',
    5 => 'OC\\Preview\\XBitmap',
    6 => 'OC\\Preview\\MarkDown',
    7 => 'OC\\Preview\\MP3',
    8 => 'OC\\Preview\\TXT',
    9 => 'OC\\Preview\\Illustrator',
    10 => 'OC\\Preview\\Movie',
    11 => 'OC\\Preview\\MSOffice2003',
    12 => 'OC\\Preview\\MSOffice2007',
    13 => 'OC\\Preview\\MSOfficeDoc',
    14 => 'OC\\Preview\\OpenDocument',
    15 => 'OC\\Preview\\PDF',
    16 => 'OC\\Preview\\Photoshop',
    17 => 'OC\\Preview\\Postscript',
    18 => 'OC\\Preview\\StarOffice',
    19 => 'OC\\Preview\\SVG',
    20 => 'OC\\Preview\\TIFF',
    21 => 'OC\\Preview\\Font',
];

// loads a space separated list with preview providers from env if specified. Else use default list (including static indices)
if (env_all_available(["FS_ENV_PREVIEW_PROVIDERS"])) {
    $CONFIG_BASE['enabledPreviewProviders'] = explode(" ", get_from_env("FS_ENV_PREVIEW_PROVIDERS"));
} else {
    $CONFIG_BASE['enabledPreviewProviders'] = $default_preview_providers;
}

$CONFIG_BASE['trusted_domains'] = explode(" ", get_from_env("FS_ENV_TRUSTED_DOMAINS", ""));
$CONFIG_BASE['trusted_proxies'] = explode(" ", get_from_env("FS_ENV_TRUSTED_PROXIES", ""));
$CONFIG_BASE['skeletondirectory'] = "";
$CONFIG_BASE['overwriteprotocol'] = get_from_env("FS_ENV_OVERWRITE_PROTOCOL", "https");

// uses the first trusted_domain (if set) if no cli overwrite url is set
$default_cli_url = "";
if (isset($CONFIG_BASE['trusted_domains'][0])) {
    $default_cli_url = $CONFIG_BASE['overwriteprotocol'] . "://" . $CONFIG_BASE['trusted_domains'][0];
}
$CONFIG_BASE['overwrite.cli.url'] = get_from_env("FS_ENV_OVERWRITE_CLI_URL", $default_cli_url);
$CONFIG_BASE['overwritehost'] = get_from_env("FS_ENV_OVERWRITE_HOST", $CONFIG_BASE['trusted_domains'][0]);
$CONFIG_BASE['enforce_theme'] = "";
$CONFIG_BASE['default_phone_region'] = "DE";
