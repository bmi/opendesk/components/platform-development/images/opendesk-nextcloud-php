<?php
// SPDX-FileCopyrightText: 2023 Dataport AöR
// SPDX-License-Identifier: EUPL-1.2
print(__FILE__ . PHP_EOL);

$redis_host = get_from_env("FS_ENV_REDIS_HOST");
$redis_port = get_from_env("FS_ENV_REDIS_PORT", 6379);
$redis_database = get_from_env("FS_ENV_PHP_REDIS_DBINDEX", 1);
$redis_password = get_from_env("FS_ENV_REDIS_PASSWORD");
$redis_username = get_from_env("FS_ENV_REDIS_USER");

$redis_connection_string = "tcp://$redis_host:$redis_port?database=$redis_database";
if ($redis_password !== false) {
    $redis_connection_string .= "&auth[pass]=$redis_password";
    if ($redis_username !== false) {
        $redis_connection_string .= "&auth[user]=$redis_username";
    }
}

if (get_from_env("FS_ENV_PHP_REDIS_FULL_URI") === false) {
    putenv("FS_ENV_PHP_REDIS_FULL_URI=$redis_connection_string");
} else {
    putenv("FS_ENV_PHP_REDIS_FULL_URI=" . get_from_env("FS_ENV_PHP_REDIS_FULL_URI"));
}

putenv("FS_ENV_PHP_WORKERS=" . get_from_env("FS_ENV_PHP_WORKERS", 20));
putenv("FS_ENV_PHP_ACCESSLOG=" . get_from_env("FS_ENV_PHP_ACCESSLOG", "/dev/null"));

$php_slowlog_file = get_from_env("FS_ENV_PHP_SLOWLOG", NC_DATA_DIR . "/log/php-slow.log");
$php_slowlog_timeout = get_from_env("FS_ENV_PHP_SLOWLOG_TIMEOUT", 0);

if ($php_slowlog_timeout != 0) {
    print("PHP Slowlog enabled. Logging to $php_slowlog_file (Timeout: $php_slowlog_timeout)" . PHP_EOL);
}

putenv("FS_ENV_PHP_SLOWLOG=$php_slowlog_file");
putenv("FS_ENV_PHP_SLOWLOG_TIMEOUT=$php_slowlog_timeout");
