<?php
// SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
// SPDX-FileCopyrightText: 2023 Dataport AöR
// SPDX-License-Identifier: EUPL-1.2
print(__FILE__ . PHP_EOL);

print("Loading config.php from database..." . PHP_EOL);
config_php_set_readwrite();
config_php_save_to_file(array_merge(config_php_load_from_db(), ["config_is_read_only" => true]));

// Create .ocdata file to signalise nextcloud is installed
if(CREATE_OCDATA_FILE) {
  $write_result = file_put_contents(NC_DATA_DIR . "/.ocdata", "");
  if ($write_result === false) {
    print("ERROR: Could not create .ocdata file." . PHP_EOL);
    exit(EXIT_CODE_DATA_DIR_NOT_WRITABLE);
  }
}

remove_can_install();

print("Waiting for redis..." . PHP_EOL);
wait_for_redis_from_config();

print("Configuring php" . PHP_EOL);

require_once "configure_php.php";

config_php_set_readonly();

// replace current process with php
$command_args = [NC_WWW_DIR . "/cron.php"];

pcntl_exec(PHP_BIN, $command_args);
