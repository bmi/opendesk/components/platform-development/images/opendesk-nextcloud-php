<?php
// SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
// SPDX-FileCopyrightText: 2023 Dataport AöR
// SPDX-License-Identifier: EUPL-1.2
print(__FILE__ . PHP_EOL);
require_once "functions.php";

config_php_set_readwrite();
db_create_table();
wait_for_redis_from_env();

if (DEV_MODE) {
    print("current database config content:" . PHP_EOL);
    var_dump(db_get_value("config_php"));
}

$nc_installed = is_nextcloud_installed();

if ($nc_installed) {
    // Create .ocdata file to signalise nextcloud is installed
    if(CREATE_OCDATA_FILE) {
        $write_result = file_put_contents(NC_DATA_DIR . "/.ocdata", "");
        if ($write_result === false) {
            print("ERROR: Could not create .ocdata file." . PHP_EOL);
            exit(EXIT_CODE_DATA_DIR_NOT_WRITABLE);
        }
    }
}

// check for inconsistencies and abort
$nc_installed_consistent = is_state_consistent($nc_installed);

// do stuff
if (!$nc_installed) {
    require_once "nextcloud_install.php";
}

remove_can_install();

require_once "apply_env_to_nc_config.php";
require_once "nextcloud_upgrade.php";
require_once "nextcloud_configure.php";

// save current config.php
print("Saving current config.php to database... " . PHP_EOL);
config_php_save_to_db(config_php_load_from_file());
