<?php
// SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
// SPDX-FileCopyrightText: 2023 Dataport AöR
// SPDX-License-Identifier: EUPL-1.2
print(__FILE__ . PHP_EOL);


$config_json_content = null;


// loads config.json if it exists
// the global variable is used as cache to not have to load the file on every call
function load_config_json() {
    global $config_json_content;

    if ($config_json_content !== null) {
        return $config_json_content;
    }

    $config_json = "/var/nextcloud/config/config.json";
    if (file_exists($config_json)) {
        $content = file_get_contents($config_json);

        // check for read errors
        if ($content === false) {
            print("Failed to read config.json." . PHP_EOL);

            exit(1); // TODO
        }

        $data = json_decode($content, true);

        // check for json errors
        if (json_last_error() != JSON_ERROR_NONE) {
            print("config.json invalid. Error:" . PHP_EOL);
            print(json_last_error_msg() . PHP_EOL);
            print("File content:" . PHP_EOL);
            print($content . PHP_EOL . PHP_EOL);

            exit(1); // TODO
        }

        $output = [];
        foreach ($data as $k => $v) {

            // ensure all keys are uppercase
            $k = strtoupper($k);

            // ensure all keys start with FS_ENV_
            if (strpos($k, "FS_ENV_") !== 0) {
                $k = "FS_ENV_" . $k;
            }

            $output[$k] = $v;
        }

        // save data into global var for later use
        $config_json_content = $output;
        return $config_json_content;

    } else {

        // return empty array if no config.json was found
        $config_json_content = [];
        return $config_json_content;
    }
}


// gets value from environment vars and sets default if set
// when a value is not set and no default is defined the script exits.
function get_from_env_required($name, $default = null) {
    $value = getenv($name);

    // no value in env --> have a look at config.json
    if ($value === false) {
        $config_json = load_config_json();
        if (isset($config_json[$name])) {
            /*if (DEV_MODE) {
                print(" * Loaded variable $name from config.json." . PHP_EOL);
            }*/
            $value = $config_json[$name];
        }
    }

    if ($value === false && $default === null) {
        print("Missing Env var: $name" . PHP_EOL);
        exit(EXIT_CODE_MISSING_ENV_VAR);
    }

    if ($value === false && $default !== null) {
        return $default;
    }
    return $value;
}


// returns value from env vars with false as default
function get_from_env($name, $default = false) {
    return get_from_env_required($name, $default);
}


// returns array (dict or list) given in env var
// supports json (optionally with base64) from env and additionally direct usage from json
function get_array_from_env($name, $default = []) {
    $raw = get_from_env($name, $default);

    // direct array usage (when loading from json)
    if (is_array($raw)) {
        return $raw;
    }

    // decode base64
    if (substr($raw, 0, 1) != "{") {
        $raw = base64_decode($raw);
    }

    // decode json
    $data = json_decode($raw, true);

    return  $data;
}


// checks if all env vars are set
function env_all_available($vars) {
    $config_json = load_config_json();

    foreach ($vars as $var) {
        if (getenv($var) === false && !isset($config_json[$var])) {
            return false;
        }
    }

    return true;
}


// simply converts a string to a boolean
function str2bool($s) {
    $s = strtolower($s);
    $s = trim($s);
    if ($s === 1 || $s === "true" || $s === true || $s === "1") {
        return true;
    }
    return false;
}


// simply converts boolean style string to "yes" or "no"
function input2yesno($s) {
    $s = strtolower($s);
    $s = trim($s);
    if ($s === 1 || $s === "true" || $s === true || $s === "1") {
        return 'yes';
    }
    return 'no';
}


// stores passwords and stuff to be filtered from the output
if (!isset($credentials)) {
    $credentials = [];
}


// adds stuff to the output filter
function add_to_masking_filter($string, $escape = true) {
    if (is_array($string)) {
        foreach ($string as $s) {
            // yay, recursion
            add_to_masking_filter($s, $escape);
        }
    }
    global $credentials;
    $credentials[] = $string;
}


// removes credentials from output
function print_safe($string, $newline = true) {
    global $credentials;
    if (!isset($credentials)) {
        $credentials = [];
    }

    foreach ($credentials as $cr) {
        $string = str_replace($cr, "***", $string);
    }

    print($string);
    if ($newline) {
        print(PHP_EOL);
    }
}


// wait until redis is ready (simple tcp port open check)
function wait_for_redis($host, $port) {
    print("Waiting for Redis..." . PHP_EOL);
    $success = false;
    $i = 1;
    $max_tries = 20;

    while ($success == false && $i <= $max_tries) {
        $f = @fsockopen($host, $port, $errno, $errstr, 30);
        if (!$f) {
            print("Redis not available ($errno: $errstr). Try $i/$max_tries" . PHP_EOL);
            sleep(2);
            $i++;
            continue;
        }

        fclose($f);
        print("Redis available." . PHP_EOL);
        $success = true;
        break;
    }

    return $success;
}


// reads the redis config from env and waits for that
function wait_for_redis_from_env() {
    $host = get_from_env_required('FS_ENV_REDIS_HOST');
    $port = intval(get_from_env_required('FS_ENV_REDIS_PORT', 6379));

    $success = wait_for_redis($host, $port);

    if (!$success) {
        print("Failed to connect to Redis." . PHP_EOL);
        exit(EXIT_CODE_REDIS_NOT_AVAILABLE);
    }
    return true;
}


// reads the redis config from config.php and waits for that
function wait_for_redis_from_config() {
    $config = config_php_load_from_file();

    if (!isset($config["redis"]["host"]) || !isset($config["redis"]["port"])) {
        print("Redis not configured." . PHP_EOL);
        return true;
    }

    $success = wait_for_redis($config["redis"]["host"], $config["redis"]["port"]);

    if (!$success) {
        print("Failed to connect to Redis." . PHP_EOL);
        exit(EXIT_CODE_REDIS_NOT_AVAILABLE);
    }
    return true;
}


// returns a pdo database connection object
function get_db_connection() {
    $type = get_from_env_required('FS_ENV_DBTYPE');
    $host = get_from_env_required('FS_ENV_DBHOST');
    $name = get_from_env_required('FS_ENV_DBNAME');
    $port = get_from_env_required('FS_ENV_DBPORT', 3306);
    $user = get_from_env_required('FS_ENV_DBUSER');
    $password = get_from_env_required('FS_ENV_DBPASSWORD');
    $options = [];
    if ($type == "mysql") {
        $options = array(
            \PDO::ATTR_PERSISTENT => TRUE,
            \PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARACTER SET utf8, NAMES utf8"
        );
    }

    $pdo = new PDO(
        "$type:host=$host;port=$port;dbname=$name",
        $user,
        $password,
        $options
    );
    return $pdo;
}


// removes a directory and all of its content
function rrmdir($dir) {
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (is_dir($dir . DIRECTORY_SEPARATOR . $object) && !is_link($dir . "/" . $object)) {
                    rrmdir($dir . DIRECTORY_SEPARATOR . $object);
                } else {
                    unlink($dir . DIRECTORY_SEPARATOR . $object);
                }
            }
        }
        rmdir($dir);
    }
}


// recursively sets the ownership of the data dir to the expected value
function data_dir_chown($dir = null) {
    if ($dir == null) {
        $dir = NC_DATA_DIR;
    }

    // ensure the uid is a int, otherwise php will try to search for a user
    $user_id = intval(UID);

    $dir = rtrim($dir, "/");
    if ($items = glob($dir . "/*")) {
        foreach ($items as $item) {
            if (is_dir($item)) {
                data_dir_chown($item);
            } else {
                chown($item, $user_id);
                chgrp($item, $user_id);
            }
        }
    }

    chown($dir, $user_id);
    chgrp($dir, $user_id);
}


// wait until the database is ready
function wait_for_db() {
    $success = false;
    $i = 1;
    $max_tries = 20;

    while ($success == false && $i <= $max_tries) {
        try {
            $pdo = get_db_connection();
            print("Database available." . PHP_EOL);
            $success = true;
            break;
        } catch (Exception $e) {
            print("Database not available. Try $i/$max_tries" . PHP_EOL);
            sleep(2);
            $i++;
        }
    }

    if (!$success) {
        print("Failed to connect to database." . PHP_EOL);
        exit(EXIT_CODE_DB_NOT_AVAILABLE);
    }

    $pdo = null;
    return true;
}


// parses base64 string that is possibly also gziipped
function extract_base64_with_zip($data) {
    $zipped = false;

    //accept gzip: prefix
    if (substr($data, 0, 5) == "gzip:") {
        $zipped = true;
        $data = substr($data, 5);
        print("Detected zipped content." . PHP_EOL);
    }

    $data = base64_decode($data);

    if ($zipped) {
        $data = gzdecode($data);
    }

    return $data;
}


// reads from the key-value-store
function db_get_value($key) {
    $pdo = get_db_connection();
    $statement = $pdo->prepare("SELECT * FROM fs_config_store WHERE name = :name");
    $statement->execute(["name" => $key]);
    $data = $statement->fetchAll(\PDO::FETCH_ASSOC);
    $pdo = null;
    if (count($data) == 0) {
        return null;
    }
    //return base64_decode($data[0]["value"]);
    return $data[0]["value"];
}


// writes to the key-value-store
function db_set_value($key, $value) {
    //$value = base64_encode($value);
    $pdo = get_db_connection();

    $type = $type = get_from_env_required('FS_ENV_DBTYPE');
    if ($type == "pgsql") {
        $query = "INSERT INTO fs_config_store (name, value) values (:name, :value) ON CONFLICT (name) DO UPDATE SET value = :value";
    } else {
        $query = "INSERT INTO fs_config_store (name, value) values (:name, :value) ON DUPLICATE KEY UPDATE value = :value";
    }

    $statement = $pdo->prepare($query);
    $statement->execute(["name" => $key, "value" => $value]);

    $pdo = null;

    return true;
}


// creates the key-value-store-table
function db_create_table() {
    $query = <<<EOL
CREATE TABLE IF NOT EXISTS fs_config_store (
    name VARCHAR(128) NOT NULL,
    value TEXT,
    PRIMARY KEY (name)
);
EOL;
    $pdo = get_db_connection();
    $result = $pdo->query($query);
    $pdo = null;
}


// get all tables in the database
function db_list_tables() {
    $type = $type = get_from_env_required('FS_ENV_DBTYPE');
    $pdo = get_db_connection();
    if ($type == "pgsql") {
        $query = $pdo->query("SELECT tablename FROM pg_catalog.pg_tables WHERE schemaname='public'");
    } else {
        $query = $pdo->query("SHOW TABLES");
    }

    $data = $query->fetchAll(PDO::FETCH_COLUMN);
    return $data;
}


// checks the current installation status based on the values set inside the config.php
function is_nextcloud_installed() {
    $config_php_current = config_php_load_from_db(true);
    $required_keys = ["version", "passwordsalt", "installed", "secret"];

    $values_found = true;

    foreach ($required_keys as $key) {
        if (!isset($config_php_current[$key])) {
            $values_found = false;
            break;
        }
    }

    // no need to search for an old config if we already found a valid one
    if ($values_found) {
        return true;
    }


    // handle existing installation
    $tables = db_list_tables();
    if (in_array("_oc_files", $tables) && db_get_value("config_php_classic_migrated") == null) {
        print("Found _oc_files table. Trying to load config.php from classic database format..." . PHP_EOL);
        try {
            $pdo = get_db_connection();
            $statement = $pdo->prepare("SELECT * FROM `_oc_files` WHERE path = :path");
            $statement->execute(["path" => "config.php"]);
            $data = $statement->fetchAll(\PDO::FETCH_ASSOC);
            if (isset($data[0]["filecontents"])) {
                $raw = $data[0]["filecontents"];
                $content = base64_decode($raw);
                file_put_contents(NC_WWW_DIR . "/config/config.php", $content);
                config_php_save_to_db(config_php_load_from_file());
                print("Done." . PHP_EOL);
                db_set_value("config_php_classic_migrated", "true");
                return true;
            }
        } catch (Exception $e) {
            print("Loading of classic config.php failed." . PHP_EOL);
        }
    }

    return false;
}


// checks if the current state is consistent (e.g. nothing inside the data dir and nc not installed)
function is_state_consistent($installed=false) {
    $ignore = false;
    if (get_from_env("FS_ENV_IGNORE_STATE_CONSISTENCY") !== false) {
        $ignore = true;
    }

    // files in data dir
    $files_data_dir = array_diff(scandir(NC_DATA_DIR), array('.', '..'));
    if (in_array(".ocdata", $files_data_dir) && $installed == false) {
        print("Found .ocdata in the data dir, but nextcloud seems not to be installed. Exiting." . PHP_EOL);
        if (!$ignore) {
            exit(EXIT_CODE_INCONSISTENT_STATE);
        } else {
            print("Not exiting, ignore mode active.");
        }
    }

    if (in_array(".ocdata", $files_data_dir) == false && $installed == true) {
        print("Nextcloud seems to be installed, but .ocdata does not exist. Exiting." . PHP_EOL);
        if (!$ignore) {
            exit(EXIT_CODE_INCONSISTENT_STATE);
        } else {
            print("Not exiting, ignore mode active.");
        }
    }

    // table count
    $tables = db_list_tables();
    $tables = array_diff($tables, ["fs_config_store"]);

    if (count($tables) > 0 && $installed == false) {
        print("Found tables in the database, but nextcloud seems not to be installed. Exiting." . PHP_EOL);
        if (!$ignore) {
            exit(EXIT_CODE_INCONSISTENT_STATE);
        } else {
            print("Not exiting, ignore mode active.");
        }
    }
    if (count($tables) == 0 && $installed == true) {
        print("Nextcloud seems to be installed, but the database seems to be empty. Exiting." . PHP_EOL);
        if (!$ignore) {
            exit(EXIT_CODE_INCONSISTENT_STATE);
        } else {
            print("Not exiting, ignore mode active.");
        }
    }

    return true;
}


// loads the config.php from file as array
function config_php_load_from_file($dir = null) {
    if ($dir == null) {
        $dir = NC_WWW_DIR;
    }

    $config_php_name = $dir . "/config/config.php";

    if (!file_exists($config_php_name)) {
        return [];
    }
    require $config_php_name;

    return $CONFIG;
}


// saves the config array to db
function config_php_load_from_db($ignore_errors = false) {
    $config_php_current = db_get_value("config_php");
    if ($config_php_current == "" || $config_php_current == false) {
        print("No config.php found in database." . PHP_EOL);
        $config_php_current = [];
        if (!$ignore_errors) {
            exit(EXIT_CODE_CONFIG_PHP_NOT_IN_DB);
        }
    } else {
        print("config.php found in database." . PHP_EOL);
        $config_php_current = json_decode($config_php_current, true);

        if (json_last_error() != JSON_ERROR_NONE) {
            print("json invalid. Error:" . PHP_EOL);
            print(json_last_error_msg() . PHP_EOL);
            $config_php_current = [];
            if (!$ignore_errors) {
                exit(EXIT_CODE_CONFIG_PHP_INVALID);
            }
        }

        if (!is_array($config_php_current)) {
            print("config_php_current is no a valid array." . PHP_EOL);
            $config_php_current = [];
            if (!$ignore_errors) {
                exit(EXIT_CODE_CONFIG_PHP_INVALID);
            }
        }
    }

    return $config_php_current;
}


// saves the config array as valid config.php to file
function config_php_save_to_file($config_php_current, $dir = null) {
    $config_php_content = "<?php" . PHP_EOL . "\$CONFIG=" . var_export($config_php_current, true) . ";" . PHP_EOL;
    if ($dir == null) {
        $dir = NC_WWW_DIR;
    }

    $config_php_name = $dir . "/config/config.php";

    file_put_contents($config_php_name, $config_php_content);
    if (DEV_MODE) {
        print("Config.php Content:" . PHP_EOL);
        print($config_php_content . PHP_EOL . PHP_EOL);
    }
}


// saves the config array as json to db
function config_php_save_to_db($config_php_current) {
    db_set_value("config_php", json_encode($config_php_current));
}


// removes write permissions from config.php
function config_php_set_readonly($dir = null) {
    if ($dir == null) {
        $dir = NC_WWW_DIR;
    }

    $config_php_name = $dir . "/config/config.php";

    chmod($config_php_name, 0440);
}


// adds write permissions to config.php
function config_php_set_readwrite($dir = null) {
    if ($dir == null) {
        $dir = NC_WWW_DIR;
    }

    $config_php_name = $dir . "/config/config.php";
    if (file_exists($config_php_name)) {
        chmod($config_php_name, 0660);
    }
}


// remove config/CAN_INSTALL if it exists
function remove_can_install($dir = null) {
    if ($dir == null) {
        $dir = NC_WWW_DIR;
    }

    if (file_exists($dir . "/config/CAN_INSTALL")) {
        if (is_writable($dir . "/config/CAN_INSTALL")) {
            unlink($dir . "/config/CAN_INSTALL");
        } else {
            print("Warning: Could not remove CAN_INSTALL-file" . PHP_EOL);
        }
    }
}


// read version from nextcloud files in image
function get_nc_version($dir = null) {
    if ($dir == null) {
        $dir = NC_WWW_DIR;
    }

    include $dir . "/version.php";
    $version = implode(".", $OC_Version);
    return $version;
}


// read version from saved config
function get_installed_nc_version() {
    $config = config_php_load_from_db();
    if (in_array("version", $config)) {
        $version = $config["version"];
        return $version;
    }

    // return some dummy value if nothing was found
    return "0.0.0.0";
}


// returns major version of version string
function get_major_version($version) {
    $version = trim($version);
    $parts = explode(".", $version);

    // when an error occurs
    if (count($parts) < 1) {
        print("get_major_version: could not parse version string. Input was: $version" . PHP_EOL);
        return null;
    }

    return $parts[0];
}


// returns an array of required major version upgrades to run before the current version can be used.
// e.g.: (24, 27) -> [25, 26]
function get_upgrade_steps($current_version, $target_version) {
    $current_major = get_major_version($current_version);
    $target_major = get_major_version($target_version);

    // current and target are the same or just one major different -> nothing to do
    if ($target_major - $current_major < 2) {
        return [];
    }

    // now calculate required steps
    $steps = [];
    for ($i = $current_major + 1; $i < $target_major; $i++) {
        $steps[] = $i;
    }

    return $steps;
}


// checks if files for a specific major release are in our image
function version_zip_exists($major_version) {
    $test_file = NC_WWW_VERSIONS_DIR . "/$major_version.zip";
    return file_exists($test_file);
}


// accepts single command and list of commands
function run_occ($commands, $ignore_fail=false, $dir = null) {

    // convert single command to list
    if (!is_array($commands)) {
        $commands = [$commands];
    }

    if ($dir == null) {
        $dir = NC_WWW_DIR;
    }

    $total_output = [];
    foreach ($commands as $command) {
        print_safe("Running occ $command...", false);
        $output = [];
        $exit_code = 1;
        exec(PHP_BIN . " " . $dir . "/occ $command 2>&1", $output, $exit_code);
        if ($exit_code != 0 && $ignore_fail == false) {
            print("FAIL." . PHP_EOL);
            print("Got exit code $exit_code. Exiting now." . PHP_EOL);
            print("Command output:". PHP_EOL);
            print(PHP_EOL);
            print_safe(implode(PHP_EOL, $output));
            print(PHP_EOL);
            exit(EXIT_CODE_OCC_ERROR);
        }
        $total_output[] = $output;
        print("Success." . PHP_EOL);
    }

    return $total_output;
}


// hashes a command or list of commands to be compared
function occ_hash_commandlist($commands) {
    if (!is_array($commands)) {
        $commands = [$commands];
    }

    $data = json_encode($commands);
    $hash = hash("sha512", $data);

    return $hash;
}


// removes all saved occ hashes, forces reexecution of all occ commands
function occ_clear_saved_states() {
    $query = "DELETE FROM fs_config_store WHERE name LIKE 'occ_%'";
    $pdo = get_db_connection();
    $pdo->query($query);
    return true;
}


// checks the list of commands against the saved hash
function occ_has_been_modified($key, $commands) {
    $db_hash = db_get_value("occ_" . $key);

    if ($db_hash === null) {
        return true;
    }
    $current_hash = occ_hash_commandlist($commands);

    if ($current_hash == $db_hash) {
        return false;
    }

    return true;
}


// saves hash for a list of commands
function occ_save_state($key, $commands) {
    $hash = occ_hash_commandlist($commands);
    db_set_value("occ_" . $key, $hash);
}


// runs occ only if things have been modified (or the commands were never run)
function run_occ_if_modified($key, $commands) {
    $modified = occ_has_been_modified($key, $commands);

    if ($modified || NC_OCC_FORCE_ALL) {
        print("Running occ commands for $key" . PHP_EOL);
        run_occ($commands);
        occ_save_state($key, $commands);
    } else {
        print("Not running occ commands for $key as there have been no modifications." . PHP_EOL);
    }
}

// read self-signed ca certificates
function import_certificates() {
  run_occ(["security:certificates:import /etc/ssl/certs/ca-certificates.crt"]);
}

// recursive copy function
function recursive_copy($src, $dst) {
    $dir = opendir($src);
    @mkdir($dst);
    foreach (scandir($src) as $file) {
        if (( $file != '.' ) && ( $file != '..' )) {
            if ( is_dir($src . '/' . $file) )
            {
                // Recursively call function for subdirectories
                recursive_copy($src . '/' . $file, $dst . '/' . $file);
            }
            else {
                copy($src . '/' . $file, $dst . '/' . $file);
            }
        }
    }
    closedir($dir);
}
