<?php
// SPDX-FileCopyrightText: 2023 Dataport AöR
// SPDX-License-Identifier: EUPL-1.2
print(__FILE__ . PHP_EOL);

$quota = get_from_env("FS_ENV_DEFAULT_QUOTA", "1 GB");

$shareapi_allow_links = input2yesno(get_from_env("FS_ENV_SHAREAPI_ALLOW_LINKS", "false"));
$shareapi_allow_mail_notification = input2yesno(get_from_env("FS_ENV_SHAREAPI_ALLOW_MAIL_NOTIFICATION", "false"));
$shareapi_allow_public_upload = input2yesno(get_from_env("FS_ENV_SHAREAPI_ALLOW_PUBLIC_UPLOAD", "false"));
$shareapi_enforce_links_password = input2yesno(get_from_env("FS_ENV_SHAREAPI_ENFORCE_LINKS_PASSWORD", "true"));
$enforcePasswordProtection = input2yesno(get_from_env("FS_ENV_ENFORCE_PASSWORD_PROTECTION", "true"));
$shareapi_default_internal_expire_date = input2yesno(get_from_env("FS_ENV_SHAREAPI_DEFAULT_INTERNAL_EXPIRE_DATE", "true"));
$shareapi_enforce_internal_expire_date = input2yesno(get_from_env("FS_ENV_SHAREAPI_ENFORCE_INTERNAL_EXPIRE_DATE", "false"));
$shareapi_internal_expire_after_n_days = get_from_env("FS_ENV_SHAREAPI_INTERNAL_EXPIRE_AFTER_N_DAYS", "90");
$shareapi_default_expire_date = input2yesno(get_from_env("FS_ENV_SHAREAPI_DEFAULT_EXPIRE_DATE", "true"));
$shareapi_enforce_expire_date = input2yesno(get_from_env("FS_ENV_SHAREAPI_ENFORCE_EXPIRE_DATE", "false"));
$shareapi_expire_after_n_days = get_from_env("FS_ENV_SHAREAPI_EXPIRE_AFTER_N_DAYS", "30");
#$link_defaultExpDays = get_from_env('FS_LINK_DEFAULT_EXP_DAYS', "30");
#$internal_defaultExpDays = get_from_env('FS_INTERNAL_DEFAULT_EXP_DAYS', "30");

$occ_commands = [
    "config:app:set files default_quota --value='$quota'",

    ## upload chunking , set 10MB
    "config:app:set files max_chunk_size --value 10485760",

    ### sharing default security settings ###
    "config:app:set core shareapi_allow_links --value='$shareapi_allow_links'",
    "config:app:set core shareapi_allow_mail_notification --value='$shareapi_allow_mail_notification'",
    "config:app:set core shareapi_allow_public_upload --value='$shareapi_allow_public_upload'",
    "config:app:set core shareapi_enforce_links_password --value='$shareapi_enforce_links_password'",
    "config:app:set sharebymail enforcePasswordProtection --value='$enforcePasswordProtection'",
    "config:app:set core shareapi_default_internal_expire_date --value='$shareapi_default_internal_expire_date'",
    "config:app:set core shareapi_enforce_internal_expire_date --value='$shareapi_enforce_internal_expire_date'",
    "config:app:set core shareapi_internal_expire_after_n_days --value='$shareapi_internal_expire_after_n_days'",
    "config:app:set core shareapi_default_expire_date --value='$shareapi_default_expire_date'",
    "config:app:set core shareapi_enforce_expire_date --value='$shareapi_enforce_expire_date'",
    "config:app:set core shareapi_expire_after_n_days --value='$shareapi_expire_after_n_days'",
    #"config:app:set core link_defaultExpDays --value='$link_defaultExpDays'",
    #"config:app:set core internal_defaultExpDays --value='$internal_defaultExpDays'",
    #"config:app:delete core shareapi_default_remote_expire_date",
    #"config:app:delete core shareapi_enforce_remote_expire_date",

    ### federation ###
    "config:app:set files_sharing lookupServerUploadEnabled --value='no'",

    ### Activity ###
    "config:app:set activity notify_email_calendar --value='0'",
    "config:app:set activity notify_email_calendar_event --value='0'",
    "config:app:set activity notify_email_calendar_todo --value='0'",
    "config:app:set activity notify_email_comments --value='0'",
    "config:app:set activity notify_email_contacts --value='0'",
    "config:app:set activity notify_email_file_changed --value='0'",
    "config:app:set activity notify_email_group_settings --value='0'",
    "config:app:set activity notify_email_public_links --value='0'",
    "config:app:set activity notify_email_remote_share --value='1'",
    "config:app:set activity notify_email_shared --value='1'",
    "config:app:set activity notify_email_systemtags --value='0'",
    "config:app:set activity notify_email_security --value='0'",
    "config:app:set activity notify_notification_calendar --value='0'",
    "config:app:set activity notify_notification_calendar_event --value='0'",
    "config:app:set activity notify_notification_calendar_todo --value='0'",
    "config:app:set activity notify_notification_comments --value='0'",
    "config:app:set activity notify_notification_contacts --value='0'",
    "config:app:set activity notify_notification_favorite --value='0'",
    "config:app:set activity notify_notification_file_changed --value='0'",
    "config:app:set activity notify_notification_file_favorite_changed --value='0'",
    "config:app:set activity notify_notification_group_settings --value='0'",
    "config:app:set activity notify_notification_personal_settings --value='0'",
    "config:app:set activity notify_notification_public_links --value='0'",
    "config:app:set activity notify_notification_remote_share --value='1'",
    "config:app:set activity notify_notification_security --value='0'",
    "config:app:set activity notify_notification_shared --value='1'",
    "config:app:set activity notify_notification_systemtags --value='0'",
    "config:app:set activity notify_notification_virus_detected --value='1'",
    "config:app:set activity notify_setting_batchtime --value='86400'",
    "config:app:set activity notify_setting_self --value='0'",
    "config:app:set activity notify_setting_selfemail --value='0'",

    ### profile stuff
    "config:app:set settings profile_enabled_by_default --value 0",
    "config:system:set profile.enabled --value=false --type=boolean",

    ### set job_interval_storage_stats to an hour
    "config:app:set --value=3600 serverinfo job_interval_storage_stats",

    ### speed settings
    "config:app:set spreed default_attachment_folder --value='/'",
];

run_occ_if_modified("base_config", $occ_commands);

$adminname = get_from_env('FS_ENV_NCADMINNAME', "ansiblencadmin");
$adminuser_enabled = str2bool(get_from_env("FS_ENV_NCADMINENABLED", "false"));

if (DEV_MODE) {
    print("Running in Dev-Mode, enabling admin user ($adminname)" . PHP_EOL);
    run_occ_if_modified("adminuser_status", ["user:enable $adminname"]);
} elseif ($adminuser_enabled) {
    print("Not in Dev-Mode, but admin user ($adminname) enabled" . PHP_EOL);
    run_occ_if_modified("adminuser_status", ["user:enable $adminname"]);
} else {
    print("Not in Dev-Mode, disabling admin user ($adminname)" . PHP_EOL);
    run_occ_if_modified("adminuser_status", ["user:disable $adminname"]);
}

$adminpassword = get_from_env('FS_ENV_NCADMINPASS');
if ($adminpassword !== false) {
    add_to_masking_filter($adminpassword);
    $old_hash = db_get_value("admin_pw_hash");
    $new_hash = hash("sha512", $adminname . ":::" . $adminpassword);

    // set password if it has changed. occ requires an env var in this case
    if ($old_hash != $new_hash) {
        print("Admin-Password given and changed, setting it now..." . PHP_EOL);
        putenv("OC_PASS=$adminpassword");
        run_occ(["user:resetpassword --password-from-env $adminname"]);
        db_set_value("admin_pw_hash", $new_hash);
        putenv("OC_PASS"); // clear env var again
    } else {
        print("Admin-Password unchanged. Skipping." . PHP_EOL);
    }
}

$config_struct_output = render_config_struct($config_struct, $config_purposes_required);

require_once "nextcloud_configure_ca_bundle.php";

require_once "nextcloud_configure_ldap.php";

require_once "nextcloud_configure_apps.php";

require_once "nextcloud_configure_theming.php";

// in case of migration do the following
// precondition: postinstallscript is present
$migration_enabled = get_from_env('FS_ENV_MIGRATE', false);
if ($migration_enabled) {
    run_occ(["files:scan --all"]);
}

print("All commands succeeded." . PHP_EOL);
