<?php
// SPDX-FileCopyrightText: 2023 Dataport AöR
// SPDX-License-Identifier: EUPL-1.2

// override app states with output from config struct
$config_app_states = $config_struct_output["app_states"];
foreach ($config_app_states as $app_name => $app_state) {
    $NC_APP_BASE_STATES[$app_name] = $app_state;
}

// configure some apps only if we are below nc 25
if (version_compare($nc_version, "25", "<")) {
    print("Nextcloud Version is below 25, enabling some more apps..." . PHP_EOL);
    $NC_APP_BASE_STATES["accessibility"] = true;
    $NC_APP_BASE_STATES["files_videoplayer"] = true;
}


// base apps
$commands = [];
foreach ($NC_APP_BASE_STATES as $appname => $state) {
    if ($state == true) {
        $commands[] = "app:enable $appname";
    } else {
        $commands[] = "app:disable $appname";
    }
}
run_occ_if_modified("apps_base", $commands);

$config_commands = $config_struct_output["commands"];

foreach ($config_commands as $block_name => $commands) {
    run_occ_if_modified("block_$block_name", $commands);
}
