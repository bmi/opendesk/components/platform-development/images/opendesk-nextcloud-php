<?php
// SPDX-FileCopyrightText: 2023 Dataport AöR
// SPDX-License-Identifier: EUPL-1.2
$ca_file = null;


if (file_exists("/var/certs/ca-bundle.crt")) {
    print("Found ca-bundle." . PHP_EOL);
    $ca_file = "/var/certs/ca-bundle.crt";
}


// env has the higher priority, but this only works for small bundles when used via env.
// this limitation does not apply when the env var is loaded from config.json
if (get_from_env("FS_ENV_CA_BUNDLE") !== false) {
    print("Found ca-bundle env-var." . PHP_EOL);
    $raw = get_from_env("FS_ENV_CA_BUNDLE");
    $data = extract_base64_with_zip($raw);

    if (!file_exists("/tmp/")) {
        mkdir("/tmp/");
    }

    file_put_contents("/tmp/ca-bundle-env.crt", $data);
    $ca_file = "/tmp/ca-bundle-env.crt";
}


if ($ca_file !== null && file_exists($ca_file)) {
    $ca_file_hash = hash("sha512", file_get_contents($ca_file));

    // only do stuff if the ca has been changed
    if (db_get_value("ca-hash") != $ca_file_hash) {

        // remove existing CAs
        $current_cas = run_occ("security:certificates --output=json");
        $current_cas = json_decode($current_cas[0][0], true);
        foreach ($current_cas as $ca) {
            $ca_name = $ca["name"];
            run_occ("security:certificates:remove $ca_name");
        }

        // import new CA
        run_occ("security:certificates:import $ca_file");


        db_set_value("ca-hash", $ca_file_hash);
    }
}
