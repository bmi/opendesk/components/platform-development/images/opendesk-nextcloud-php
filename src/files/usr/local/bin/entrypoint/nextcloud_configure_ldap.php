<?php
// SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
// SPDX-FileCopyrightText: 2023 Dataport AöR
// SPDX-License-Identifier: EUPL-1.2

// gets the ldap config name straight from occ
function get_ldap_config_name() {
    $output = run_occ(["ldap:show-config --output=json"], true);
    if (DEV_MODE) {
        print("OCC-Output:" . PHP_EOL);
        print_r($output);
    }

    foreach ($output[0] as $line) {
        $line = trim($line);
        if ($line == "") {
            continue;
        }
        $data = json_decode($line, true);
        if (is_array($data)) {

            // check if the array contains ldapBase to filter out some nextcloud logging stuff
            foreach ($data as $k => $v) {
                if (!is_array($v)) {
                    continue 2;
                }

                $keys = array_keys($v);
                if (!in_array("ldapBase", $keys)) {
                    continue 2;
                }
            }

            // return first key if everything is allright
            $keys = array_keys($data);
            if (count($keys) > 0 && strlen($keys[0]) > 0) {
                return $keys[0];
            }
        }
    }
    return false;
}


// converts some exported variables
function convert_ldap_config_value($key, $value) {
    switch ($key) {
        case "ldapAttributesForUserSearch":
        case "ldapAttributesForGroupSearch":
            if (!is_array($value)) {
                $value = [$value];
            }
            $value = implode(";", $value);
            break;
        case "ldapBase":
        case "ldapBaseGroups":
        case "ldapBaseUsers":
        case "ldapGroupFilterObjectclass":
        case "ldapUserFilterObjectclass":
        case "ldapAdminGroup":
            if (!is_array($value)) {
                $value = [$value];
            }
            $value = $value[0];
            break;
        default:
            break;
    }

    return $value;
}


if (env_all_available(["FS_ENV_LDAP"])) {
    $data = get_array_from_env("FS_ENV_LDAP");

    $config_values_base = [
        "ldapAdminGroup" => null,
        "ldapAgentPassword" => null,
        "ldapAgentName" => null,
        "hasMemberOfFilterSupport" => "1",
        "lastJpegPhotoLookup" => "0",
        "ldapAttributesForUserSearch" => "sn;givenname;uid ",
        //"ldapBackupHost" => null,
        //"ldapBackupPort" => null,
        "ldapBase" => null,
        "ldapBaseGroups" => null,
        "ldapBaseUsers" => null,
        "ldapCacheTTL" => "600",
        "ldapConfigurationActive" => "1",
        "ldapEmailAttribute" => "mailPrimaryAddress",
        "ldapExperiencedAdmin" => "0",
        "ldapExpertUsernameAttr" => "entryUUID",
        "ldapGidNumber" => "gidNumber",
        "ldapGroupDisplayName" => "cn",
        "ldapGroupFilter" => "'(&(objectClass=opendeskFileshareGroup)(opendeskFileshareEnabled=TRUE))'",
        "ldapGroupFilterMode" => "1",
        "ldapGroupFilterObjectclass" => "opendeskFileshareGroup",
        "ldapGroupMemberAssocAttr" => "uniqueMember",
        "ldapHost" => null,
        "ldapLoginFilter" => "'(&(opendeskFileshareEnabled=TRUE)(entryUUID=%uid))'",
        "ldapLoginFilterEmail" => "0",
        "ldapLoginFilterMode" => "1",
        "ldapLoginFilterUsername" => "1",
        "ldapNestedGroups" => "0",
        "ldapPagingSize" => "500",
        "ldapPort" => null,
        "ldapTLS" => "0",
        "ldapUserAvatarRule" => "default",
        "ldapUserDisplayName" => "displayname",
        "ldapUserDisplayName2" => "mailPrimaryAddress",
        "ldapUserFilter" => "'(opendeskFileshareEnabled=TRUE)'",
        "ldapUserFilterMode" => "1",
        "ldapUserFilterObjectclass" => "opendeskFileshareUser",
        "ldapUuidGroupAttribute" => "auto",
        "ldapUuidUserAttribute" => "auto",
        "turnOffCertCheck" => "0",
        "turnOnPasswordChange" => "0",
        "useMemberOfToDetectMembership" => "1"
    ];
    $config_values = [];
    foreach ($config_values_base as $k => $v) {
        // use provided config if present
        if (isset($data[$k])) {
            $config_values[$k] = $data[$k];
            continue;
        }

        // no default set? -> fail
        if ($v === null) {
            print("LDAP Configuration error: Missing value $k" . PHP_EOL);
            exit(EXIT_CODE_CONFIG_LDAP_ERROR);
        }

        // use default value
        $config_values[$k] = $v;
    }

    add_to_masking_filter($config_values["ldapAgentPassword"]);

    // convert some values when an exported config has been directly used
    foreach ($config_values as $k => $v) {
        $config_values[$k] = convert_ldap_config_value($k, $v);
    }

    if (DEV_MODE) {
        print("LDAP config_values:" . PHP_EOL);
        print_r($config_values);
        print(PHP_EOL);
    }

    run_occ_if_modified("auth_ldap_state", ["app:enable user_ldap"]);
    $config_name = get_ldap_config_name();
    if ($config_name === false) {
        run_occ(["ldap:create-empty-config"]);
        $config_name = get_ldap_config_name();
    }

    $ldap_occ_prefix = "ldap:set-config $config_name";

    $ldap_occ_commands = [];

    foreach ($config_values as $k => $v) {
        $v = trim($v, "'");
        $ldap_occ_commands[] = "$ldap_occ_prefix $k '$v'";
    }

    run_occ_if_modified("auth_ldap", $ldap_occ_commands);
} else {
    run_occ_if_modified("auth_ldap_state", ["app:disable user_ldap"]);
}
