<?php
// SPDX-FileCopyrightText: 2023 Dataport AöR
// SPDX-License-Identifier: EUPL-1.2

// write an empty png image to the specified target
function create_dummy_image($filename) {
    $im = imagecreatetruecolor(1920, 1080);
    imagepng($im, $filename);
    imagedestroy($im);
}

# a dummy image is created and set as background. After that its just removed again.
# this way the default background image is getting completely removed.
# feels hacky, but nextcloud says this is the way to do it... ¯\_(ツ)_/¯

create_dummy_image(NC_DATA_DIR . "/bg_temp.png");

$theming_commands = [];

$theming_commands[] = "theming:config background " . NC_DATA_DIR . "/bg_temp.png";
$theming_commands[] = "theming:config background --reset";

if (version_compare($nc_version, "25", ">=")) {
    $theming_commands[] = "theming:config disable-user-theming yes";
}

// this variable should contain a base64 encoded svg file. its possible to gzip the svg before base64-encoding it.
// cat phoenix_suite_logo.svg | gzip | base64 -w 0
if (env_all_available(["FS_ENV_THEMING_LOGO_SVG_BASE64"])) {
    $data = get_from_env("FS_ENV_THEMING_LOGO_SVG_BASE64");

    // check if logo has been modified
    $current_hash = hash("sha512", $data);
    $tmp_file = NC_DATA_DIR . "/logo_$current_hash.svg";

    $data = extract_base64_with_zip($data);

    file_put_contents($tmp_file, $data);
    run_occ_if_modified("theming_logo", ["theming:config logo $tmp_file"]);
    unlink($tmp_file);
} elseif (file_exists("/var/nextcloud/config/")) {
    print("found /var/nextcloud/config/ dir, looking for logo files..." . PHP_EOL);

    // list files, remove . and .. from output (just php things...)
    $files = scandir("/var/nextcloud/config/");
    $files = array_diff($files, [".", ".."]);
    $logo_file = null;

    foreach ($files as $file) {
        if (strpos($file, "logo.") !== 0) {
            continue;
        }

        print("Found a logo file: $file" . PHP_EOL);
        $logo_file = "/var/nextcloud/config/" . $file;
        break;
    }

    if ($logo_file != null) {
        // calculate file hash to check for modifications
        $current_hash = hash("sha512", file_get_contents($logo_file));
        run_occ_if_modified("theming_logo", ["theming:config logo $logo_file"]);
    } else {
        print("No logo file found." . PHP_EOL);
    }
}


if (env_all_available(["FS_ENV_THEMING_COLOR"])) {
    $theming_commands[] = "theming:config color '" . get_from_env("FS_ENV_THEMING_COLOR") . "'";
}

run_occ_if_modified("theming", $theming_commands);

if (file_exists(NC_DATA_DIR . "/bg_temp.png"))  {
    unlink(NC_DATA_DIR . "/bg_temp.png");
}
