<?php
// SPDX-FileCopyrightText: 2023 Dataport AöR
// SPDX-License-Identifier: EUPL-1.2
print(__FILE__ . PHP_EOL);

print("Running installation steps..." . PHP_EOL);
setlocale(LC_CTYPE, "en_US.UTF-8");

$dbtype = escapeshellarg(get_from_env('FS_ENV_DBTYPE'));
$dbhost = escapeshellarg(get_from_env('FS_ENV_DBHOST'));
$dbport = get_from_env('FS_ENV_DBPORT', 3306);
$dbname = escapeshellarg(get_from_env('FS_ENV_DBNAME'));
$dbuser = escapeshellarg(get_from_env('FS_ENV_DBUSER'));
$dbpassword_raw = get_from_env('FS_ENV_DBPASSWORD');
$dbpassword = escapeshellarg($dbpassword_raw);
$adminname_raw = get_from_env('FS_ENV_NCADMINNAME', "ansiblencadmin");
$adminname = escapeshellarg($adminname_raw);
$random_password = bin2hex(openssl_random_pseudo_bytes(16));
$adminpassword_raw = get_from_env('FS_ENV_NCADMINPASS', $random_password);
$adminpassword = escapeshellarg($adminpassword_raw);
$datadir = escapeshellarg(get_from_env('FS_ENV_DATA_DIR', NC_DATA_DIR));

add_to_masking_filter($adminpassword_raw);
add_to_masking_filter($dbpassword_raw);

// required for read-only-image
config_php_save_to_file(["appstoreenabled" => false]);

$command = "maintenance:install --database $dbtype --database-host $dbhost --database-port $dbport --database-name $dbname --database-user $dbuser --database-pass $dbpassword --admin-user $adminname --admin-pass $adminpassword --data-dir $datadir";
print_safe("Executing command: $command" . PHP_EOL);

exec(PHP_BIN . " " . NC_WWW_DIR . "/occ $command 2>&1", $output, $exit_code);

if ($exit_code != 0) {
    print("Got exit code $exit_code. Exiting now." . PHP_EOL);
    print("Command output:". PHP_EOL);
    print(PHP_EOL);
    print_safe(implode(PHP_EOL, $output) . PHP_EOL);
    print(PHP_EOL);
    exit(EXIT_CODE_INSTALLATION_FAILED);
}

// output admin credentials only in dev mode, also save them to database
if (DEV_MODE) {
    print("Admin Login: $adminname_raw:$adminpassword_raw" . PHP_EOL);
    db_set_value("admin_user", $adminname_raw);
    db_set_value("admin_pw", $adminpassword_raw);
}

config_php_save_to_db(config_php_load_from_file());
print("Success." . PHP_EOL);
