<?php
// SPDX-FileCopyrightText: 2023 Dataport AöR
// SPDX-License-Identifier: EUPL-1.2
print(__FILE__ . PHP_EOL);



// do step-upgrades if required
$current_version = get_installed_nc_version();
$target_version = get_nc_version();

$steps = get_upgrade_steps($current_version, $target_version);

if (count($steps) > 0) {
    print(PHP_EOL . "Required intermediate upgrade steps: " . implode(" ", $steps) . PHP_EOL );
}

// loop over major versions between the current and target version so nextcloud doesnt complain
foreach ($steps as $version) {
    print("Doing Upgrade to major version $version..." . PHP_EOL);

    if (!version_zip_exists($version)) {
        print("Files for major version $version not found. Exiting." . PHP_EOL);
        exit(1);
    }

    print("Extracting files from version zip..." . PHP_EOL);
    $dir = NC_WWW_VERSIONS_DIR . "/$version";

    mkdir($dir);
    $zip = new ZipArchive;
    $res = $zip->open(NC_WWW_VERSIONS_DIR . "/$version.zip");
    if ($res === TRUE) {
        $zip->extractTo($dir);
        $zip->close();
        print("Done." . PHP_EOL);
    } else {
        print("Extraction failed." . PHP_EOL);
        exit(1);
    }



    print(" Exact version: " . get_nc_version($dir) . PHP_EOL);

    // copy current config to version dir
    copy(NC_WWW_DIR . "/config/config.php", "$dir/config/config.php");

    // execute upgrade
    $output = run_occ("upgrade --no-interaction", false, $dir);
    if (DEV_MODE) {
        print("OCC Output: " . PHP_EOL);
        print(implode("\n  ", $output[0]) . PHP_EOL);
    }

    // copy config back to current dir
    copy("$dir/config/config.php", NC_WWW_DIR . "/config/config.php");

    print("Removing extracted nextcloud..." . PHP_EOL);
    rrmdir($dir);

    print("Done (NC$version)" . PHP_EOL);
}

print(PHP_EOL);


// run upgrade on current version
$output = run_occ("upgrade --no-interaction");

$run_post_upgrade_commands = true;
if (isset($output[0][0])) {
    if ($output[0][0] == "Nextcloud is already latest version") {
        $run_post_upgrade_commands = false;
    }
}


// clear occ state after upgrade to re-execute everything
if ($run_post_upgrade_commands) {
    occ_clear_saved_states();
}


// after the initial installation the upgrade command has nothing to do, so we need to explicitly force
// the post-upgrade commands in that case
if (isset($nc_installed)) {
    if (!$nc_installed) {
        $run_post_upgrade_commands = true;
    }
}

if (get_from_env("FS_ENV_FORCE_UPGRADE") !== false) {
    $run_post_upgrade_commands = true;
}

$post_upgrade_commands = [
    "db:convert-filecache-bigint --no-interaction",
    "db:add-missing-primary-keys --no-interaction",
    "db:add-missing-indices --no-interaction",
    "db:add-missing-columns --no-interaction"
];

if ($run_post_upgrade_commands) {
    print("Running post upgrade commands..." . PHP_EOL);
    run_occ($post_upgrade_commands);
} else {
    print("Skipping post upgrade commands." . PHP_EOL);
}

print("All commands succeeded." . PHP_EOL);
